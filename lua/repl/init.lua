-- idea from https://github.com/Vigemus/iron.nvim
local nvim = vim.api
local repl = {
	memory = {},
	core = {},
	ll = {},
}

local ext = {
	strings = require("repl.util.strings"),
}

repl.ll.new_repl_window = function(buff)
	nvim.nvim_command("botright vertical 100 split | " .. buff)
end

repl.ll.create_new_repl = function(ft, repl_cmd)
	repl.ll.new_repl_window("enew")
	local job_id = nvim.nvim_call_function('termopen', {repl_cmd})
	local bufnr = nvim.nvim_call_function('bufnr', {'%'})
	nvim.nvim_command("wincmd p")
	local inst = {
		bufnr = bufnr,
		job = job_id,
		repldef = repl_cmd
	}
	repl.memory[ft] = inst
	return inst
end


repl.ll.send_to_repl = function(ft, data)
	local dt = data
	if type(data) == "string" then
		dt = ext.strings.split(data, '\n')
		ext.strings.clean(dt)
		table.insert(dt, '')
	end

	local mem = repl.memory[ft]
	nvim.nvim_call_function('chansend', {mem.job, dt})
end

repl.ll.ensure_repl_exists = function(ft)
	if repl.memory[ft] == nil  then
		local repl_cmd = nvim.nvim_eval('g:repl_def["'.. ft .. '"]')
		repl.ll.create_new_repl(ft, repl_cmd)
	else
		local mem = repl.memory[ft]
		if nvim.nvim_call_function('bufexists', {mem.bufnr}) == 0 then
			local repl_cmd = nvim.nvim_eval('g:repl_def["'.. ft .. '"]')
			repl.ll.create_new_repl(ft, repl_cmd)
		end
	end
end


repl.core.repl = function(ft)
	ft = ft or nvim.nvim_eval('&filetype')
	local repl_cmd = nvim.nvim_eval('g:repl_def["'.. ft .. '"]')
	return repl.ll.create_new_repl(ft, repl_cmd)
end

repl.core.test = function()
	print('debug')
end


repl.core.send = function(data, ft)
	ft = ft or nvim.nvim_eval('&filetype')
	repl.ll.ensure_repl_exists(ft)
	repl.ll.send_to_repl(ft, data)
end

return repl

