local strings = {}

--strings.split = function(str, sep)
   --local fields = {}
   --local pattern = string.format("([^%s]+)", sep)
   --str:gsub(pattern, function(c) fields[#fields+1] = c end)
   --return fields
--end

function strings.split(inputstr, sep)
	sep=sep or '%s'
	local t={}
	for field,s in string.gmatch(inputstr, "([^"..sep.."]*)("..sep.."?)") do
		table.insert(t,field) 
		if s=="" then 
			return t 
		end 
	end 
end

function strings.clean(val)
	--- remove the empty line that broke the indentation
	--
	--
	local i=1
	local iv = -1
	local indent = -1
	while i <= #val-2 do
		iv = strings.indentLvl(val[i])
		if iv < 0 and strings.indentLvl(val[i+1]) ~= 0 then
			table.remove(val, i)
			--table.insert(val, i, string.format("test remove i %i iv %i ivp1 %i", i, iv, strings.indentLvl(val[i+1])))
		else
			indent = iv
			i = i +1
		end
	end
end

function strings.indentLvl(inputstr)
	local indent = 0
	for i = 1, #inputstr do
		if (string.sub(inputstr, i, i) == " ") then
			indent = indent + 1
		else
			return indent
		end
	end
	return -1
end


return strings
