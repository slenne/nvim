
 
if !has('python3')
    s:ErrMsg( "Error: Required vim compiled with +python" )
    finish
endif


"function! SwapParams(directionName)
"python << EOF
"if __name__ == '__main__':
"    import vim
"    (row,col) = vim.current.window.cursor
"    line = vim.current.buffer[row-1]
"    let old_word = expand("<cword>")
"    try:
"        (line, newCol) = Swap(line,col)
"        vim.current.buffer[row-1] = line
"        vim.current.window.cursor = (row, newCol)
"    except Exception, e:
"        print e
"EOF
"endfunction

"noremap gw :call SwapParams("forwards")<cr>
""map gw @='gs'<cr>
"
function! SynoVim()
python3 << PYEND
if __name__ == '__main__':
	import vim
	from nltk.corpus import wordnet as wn
	def python_input(message):
		vim.command('call inputsave()')
		vim.command("let user_input = input('" + message + ": ')")
		vim.command('call inputrestore()')
		return vim.eval('user_input')
	
	(row,col) = vim.current.window.cursor
	line = vim.current.buffer[row-1]
	sh =0 
	while (col-sh)> 0 and line[col-sh] != ' ':
	    sh += 1 

	word_start = col-sh+1
	sh =0 
	while (col+sh) < len(line) and line[col+sh] != ' ':
	    sh += 1 

	word_end =col+sh
	word = line[word_start:word_end]
	syn = wn.synsets(word)
	l = [ '{}: {}:\n{}'.format(i, j.name(), j.definition()) for i,j in enumerate(syn)]
	try:
		res = int(python_input('\n\n'.join(l)+'\n'))
		syn = syn[res].lemma_names()
		l = [ '{}: {}'.format(i, x) for i,x in enumerate(syn)]
		res = int(python_input('\n'+'\n'.join(l)+'\n'))
		vim.current.buffer[row-1] = line[:word_start] + syn[res] + line[word_end:]
	except:
		pass
PYEND
endfunction
"call DefPython()
noremap zs :call SynoVim()<cr>

