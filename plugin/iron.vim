
function! g:GetInnerMark()
    let lnum1 = search("^#-.-",'nb') + 1
    let lnum2 = search("^#---",'n') - 1
    let curline = line('.')
    if lnum1 > curline
        let lnum1 = 0
    endif
    if lnum2 < curline
        let lnum2 = line('$')
    endif
    let lines = getline(lnum1, lnum2)
    if lines[-1] != ""
            let lines = add(lines, "")
    endif
    let lines = add(lines, "")
    return join(lines, "\n")
endfunction

" thanks to @xolox on stackoverflow
function! g:GetVisualSelection()
    let [lnum1, col1] = getpos("'<")[1:2]
    let [lnum2, col2] = getpos("'>")[1:2]
    let lines = getline(lnum1, lnum2)
    let lines[-1] = lines[-1][: col2 - (&selection == "inclusive" ? 1 : 2)]
    let lines[0] = lines[0][col1 - 1:]
    if lines[-1] != ""
            let lines = add(lines, "")
    endif
    return join(lines, "\n")
endfunction

let g:repl_def = {'python' : '/usr/bin/python3', 'gp' : 'gnuplot', 'julia' : 'julia', 'haskell' : 'ghci', 'sh': '/bin/zsh', 'matlab' : 'octave', 'wls': 'MathKernel'}

noremap <silent> K :lua require("repl").core.send(vim.api.nvim_get_current_line())<cr>
vnoremap <silent> K :lua require("repl").core.send(vim.api.nvim_call_function('GetVisualSelection',{}))<cr>
noremap <silent> <F5> :lua require("repl").core.send(vim.api.nvim_call_function('GetInnerMark',{}))<cr>
