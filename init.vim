" Ne pas assurer la compatibilité avec l'ancien Vi
set nocompatible

" Specify a directory for plugins (for Neovim: ~/.local/share/nvim/plugged)
call plug#begin('~/.local/share/nvim/plugged')

"Plug 'bfredl/nvim-ipy'
"Plug 'vim-syntastic/syntastic'
"Plug 'hkupty/iron.nvim'
"Plug 'Vigemus/iron.nvim'
Plug 'David-Kunz/gen.nvim'
Plug 'Shougo/deoplete.nvim'
Plug 'zchee/deoplete-jedi'
Plug 'scrooloose/nerdcommenter'
Plug 'SirVer/ultisnips'
Plug 'junegunn/vim-easy-align'
Plug 'lervag/vimtex'
Plug 'tpope/vim-surround' 
"Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'JuliaEditorSupport/julia-vim'

" Initialize plugin system
call plug#end()



"changer de fenetre
nnoremap <Left> <C-w>h
nnoremap <Down> <C-w>j
nnoremap <Up> <C-w>k
nnoremap <Right> <C-w>l

"comment and uncomment
map <F3> 2\ci
"split sur la droite
set splitright

" désactive de décendre une ligne par C-J
let g:BASH_Ctrl_j = 'off'
inoremap <C-j> <NOP>

" open avec gx
let g:netrw_browsex_viewer = 'xdg-open-background.sh'

"correction orthographe
setlocal spell spelllang=fr
inoremap <C-K> <C-G>u<Esc>[s1z=`]a<C-G>u
noremap <C-K> <C-G>u<Esc>[s1z=`]a<C-G>u
set nospell

"quiter le mode terminal avec ESC
tnoremap <Esc> <C-\><C-n>

"indentation sur 4 espace
"set expandtab ts=4 sw=4 ai

"pathogen
"execute pathogen#infect()

"surlignage recherche
noremap <silent> g/ :set hls! <ENTER>
set nohls

"ne coupe pas les mots dans le retour auto a la ligne
set wrap lbr

" on désactive le mappage python
"let g:nvim_ipy_perform_mappings = 0

"buffer plus rapide
map gb :b 
map gB :b # <ENTER>

noremap Y y$

"switch 2 W
nmap <silent> gs "_yiw:s/\(\%#\w\+\)\(\_W\+\)\(\w\+\)/\3\2\1/<CR><C-o>:noh<CR>
"paste avec shift insert en gui
if has('gui_running')
    imap <S-insert> "*p
endif

" Active la coloration syntaxique
syntax on
" Définit le jeu de couleurs utilisé
" Les jeux de couleur disponibles sont les fichiers avec l'extension .vim
" dans le répertoire /usr/share/vim/vimcurrent/colors/
colorscheme desert

" Affiche la position du curseur 'ligne,colonne'
set ruler
" Affiche une barre de status en bas de l'écran
set laststatus=1
" Contenu de la barre de status
set statusline=%<%f%h%m%r%=%l,%c\ %P

" Largeur maxi du texte inséré
" '72' permet de wrapper automatiquement à 72 caractères
" '0' désactive la fonction
set textwidth=0

" Nombre de commandes dans l'historique
set history=50
" Options du fichier ~/.viminfo
set viminfo='20,\"50
" Active la touche Backspace
set backspace=2
" Autorise le passage d'une ligne à l'autre avec les flèches gauche et droite
set whichwrap=<,>,[,]
" Garde toujours des lignes visibles à l'écran au dessus du curseur
set scrolloff=2
" Affiche les commandes dans la barre de status
set showcmd
" Affiche la paire de parenthèses
set showmatch
" Essaye de garder le curseur dans la même colonne quand on change de ligne
set nostartofline
" Option de la complétion automatique
set wildmode=longest,list
" Par défaut, ne garde pas l'indentation de la ligne précédente
" quand on commence une nouvelle ligne
set noautoindent
" Options d'indentation pour un fichier C
set cinoptions=(0

" xterm-debian est un terminal couleur
"    set t_Co=256
"    set t_Sf=[3%dm
"    set t_Sb=[4%dm

" Décommentez les 2 lignes suivantes si vous voulez avoir les tabulations et
" les espaces marqués en caractères bleus
"set list
"set listchars=tab:>-,trail:-

" Les recherches ne sont pas 'case sensitives'
set ignorecase

" Le découpage des folders se base sur l'indentation
" set foldmethod=indent
" 12 niveaux d'indentation par défaut pour les folders
" set foldlevel=12

" Recherches incrémentalees
set incsearch

set number

noremap <silent> <Space> :silent noh<Bar>echo<CR>

" go middle line
noremap <silent> gm :call cursor(0, virtcol('$')/2)<cr>
noremap <silent> g1 :call cursor(0, virtcol('$')/10)<cr>
noremap <silent> g2 :call cursor(0, 2*virtcol('$')/10)<cr>
noremap <silent> g3 :call cursor(0, 3*virtcol('$')/10)<cr>
noremap <silent> g4 :call cursor(0, 4*virtcol('$')/10)<cr>
noremap <silent> g5 :call cursor(0, 5*virtcol('$')/10)<cr>
noremap <silent> g6 :call cursor(0, 6*virtcol('$')/10)<cr>
noremap <silent> g7 :call cursor(0, 7*virtcol('$')/10)<cr>
noremap <silent> g8 :call cursor(0, 8*virtcol('$')/10)<cr>
noremap <silent> g9 :call cursor(0, 9*virtcol('$')/10)<cr>

noremap <silent> <F2> <ESC>:vertical resize 80<ENTER> 
noremap <silent> <C-n> :tab split <ENTER>

nnoremap m* :let @/ = '\<' . expand('<cword>') . '\>'<cr>"

"mettre le nom du fichier dans le term
set title

filetype on
filetype plugin on
filetype indent on

""ultisnip

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<S-tab>"


" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" deoplete settings
let g:deoplete#enable_at_startup = 1

