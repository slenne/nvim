if !has('python3')
    s:ErrMsg( "Error: Required vim compiled with +python3" )
    finish
endif


function! CompAuto()


python3 << EOF

list_double = ['frac', 'sfrac', 'braket', 'ketbra']
list_tri = ['mel']

def deter_word(line_sp, col):
    p = 0
    i = col - len(line_sp[p]) 
    while i > 0 :
        p += 1
        i -= len(line_sp[p]) +1
    return p

def filtre_vide(li, n, p):
    """n le nombre de mot filtré et p la position du mot current"""
    t = True
    nb_ocur = 0
    while t :
        t = False
        for i in range(n):
            if not bool(li[p-i]):
                li.pop(p-i)
                t = True
                nb_ocur += 1
                p -= 1
    return li, p ,nb_ocur

# fonction general
def double_param(line_sp, wn):
    concat = " \\" + line_sp[wn-2] + "{" + line_sp[wn-1] + "}" +"{" + line_sp[wn] + "} "
    debut = " ".join(line_sp[:wn-2])
    fin = " ".join(line_sp[wn+1:])
    return debut + concat + fin,  1

def tri_param(line_sp, wn):
    concat = " \\" + line_sp[wn-3] + "{" + line_sp[wn-2] + "}" + "{" + line_sp[wn-1] + "}"+"{" + line_sp[wn] + "} "
    debut = " ".join(line_sp[:wn-3])
    fin = " ".join(line_sp[wn+1:])
    return debut + concat + fin,  2

def defaut_param(line_sp, wn):
    sh = -2
    if len(line_sp[wn-1]) == 1 :
        w1 = "_" + line_sp[wn-1] 
    else :
        w1 = "_{" + line_sp[wn-1] + "}"
        sh += 2
    if len(line_sp[wn]) == 1 :
        w2 = "^" + line_sp[wn] 
    else :
        w2 = "^{" + line_sp[wn] + "}"
        sh += 2
    debut = " ".join(line_sp[:wn-1])
    fin = " " + " ".join(line_sp[wn+1:])
    return debut + w1 + w2 + fin,  sh

def selec_fonction(line, wn):
    li = []
    i = wn
    for _ in range(4):
        while not bool(line[i]) :
            i -=1
        li.append(line[i])
        i -= 1
    if li[2] in list_double :
        return double_param, 3
    elif li[3] in list_tri :
        return tri_param, 4
    else :
        return defaut_param, 2

if __name__ == '__main__':
    import vim
    (row, col) = vim.current.window.cursor
    line_befor = vim.current.buffer[row-1].split(' ')
    p = deter_word(line_befor, col)
    f, nb_filtre = selec_fonction(line_befor, p)
    lif, p, nb_shift = filtre_vide(line_befor, nb_filtre, p)

    vim.current.buffer[row-1], dec = f(lif, p)
    shift = dec - nb_shift + 2
    vim.current.window.cursor = (row, col+ shift)
EOF
endfunction

inoremap `1 <ESC>:call CompAuto()<cr>a

