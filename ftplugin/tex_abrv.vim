if !has('python3')
    s:ErrMsg( "Error: Required vim compiled with +python" )
    finish
endif


function! CompAbrv()

python3 << EOF

sep = list(" ,_^$(){}[]")

def get_word_pos(li, c):
    while li[c] in sep :
        c -= 1
    p_deb = c
    while p_deb != -1 and not li[p_deb] in sep :
        p_deb -= 1
    p_fin = c
    while not li[p_fin] in sep :
        p_fin += 1
        if p_fin == len(li):
            li = li + " "
    return p_deb+1, p_fin, li
    
if __name__ == '__main__':
    import vim
    import os.path
    cwd = vim.eval("getcwd()")
    my_file_abrv = cwd + "/abrv.tex"
    if  os.path.isfile(my_file_abrv) :
        (row, col) = vim.current.window.cursor
        line = vim.current.buffer[row-1]

        p_deb, p_fin, line = get_word_pos(line, col)
        kword = line[p_deb : p_fin]
        
        t = None
        with open(my_file_abrv, "r") as f:
            for val in f :
                val = val.strip()
                try :
                    p = val.index(' ')
                    if kword == val[:p] :
                        t = val[p+1:].strip()
                        break
                except :
                    pass
                
        if not t is None :
            shift = len(t) - len(kword) 
            vim.current.buffer[row-1] = line[:p_deb] + t + line[p_fin:]
            vim.current.window.cursor = (row, p_fin+ shift)
EOF
endfunction

" inoremap <C-L> <ESC>:call CompAbrv()<cr>i
" vnoremap <C-L> :call CompAbrv()<cr>h

