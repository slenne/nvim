let g:vim_markdown_math = 1
let g:vim_markdown_no_default_key_mappings = 1
map gx <Plug>Markdown_OpenUrlUnderCursor

"compter \ dans le mots pour deoplete
set iskeyword+=\
call deoplete#custom#option("keyword_patterns", {'_': '[\\\w]+'})
