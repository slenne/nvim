"espace insécable
map   \ 

" fold
setlocal foldmethod=expr
setlocal foldexpr=GetPotionFold()


function! GetPotionFold()
    if getline(v:lnum)[0:3] == '#---' || getline(v:lnum)[0:3] == '#-*-'
        return ">1"
    endif

    return '='
endfunction


"virgule et equal convention
nmap <silent> g= :silent! s/^\([^=\(]*[^(= ]\) *= *\([^=]\)/\1 = \2/ \|silent! s/ *, */, /g<RETURN>
vmap <silent> g= :<C-u>silent!'<,'> s/^\([^=\(]*[^(= ]\) *= *\([^=]\)/\1 = \2/ \|silent!'<,'> s/ *, */, /g<RETURN>
