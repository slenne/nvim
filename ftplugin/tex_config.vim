"quelque commande latex perso
set spell spelllang=en
"let g:vimtex_view_general_viewer = 'zathura'
let g:vimtex_view_method = 'sioyek'
"indentation sur 4 espace
set expandtab ts=4 sw=4 ai


imap `2 \sqrt{{
imap <> \mean{{
imap \{} \{\}<ESC>hi
imap << \ll
imap <= \leq
imap >= \geq
imap >> \gg
imap `. \cdot
imap \vb \vb{{
imap == &=
imap → \to 
imap † ^{\dag}
imap ⇒ \Rightarrow
imap ⇐ \Leftarrow
imap ⇑ \Leftrightarrow
imap ↑ \uparrow
imap ↓ \downarrow
imap \[] \[<ENTER>\]<ESC>O
imap \|> \ket{{
imap \|< \bra{{
imap ± \pm
imap ≠ \neq

iab dvpt développement
iab qte quantité
iab fct fonction
iab cad c'est à dire

"On map l'espace insécable en ~
imap   ~

set tw=72 fo=cq wm=0

"swap latex
map gw [{d%%p

"compter \ dans le mots
set iskeyword+=\


call deoplete#custom#var('omni', 'input_patterns', {
			\ 'tex': g:vimtex#re#deoplete
			\})

let g:vimtex_quickfix_ignore_filters = [
      \ 'Underfull',
      \ 'Overfull',
      \]

"vimtex
"let g:vimtex_latexmk_options="-pdf -pdflatex='xelatex -file-line-error -shell-escape -synctex=1'"
"let g:vimtex_latexmk_continuous=0
"let g:vimtex_fold_enabled=1
"
set foldmethod=expr
set foldexpr=vimtex#fold#level(v:lnum)
set foldtext=vimtex#fold#text()
